CFLAGS = -target x86_64-none-elf -ffreestanding -fPIC -fno-stack-protector -nostdinc -nostdlib
CARPFLAGS = --no-core --no-profile --generate-only

all: kernel.elf kernel.img

main.o: kernel.carp
	carp -b kernel.carp ${CARPFLAGS}
	clang ${CFLAGS} -c out/main.c -o main.o

kernel.elf: main.o link.ld
	mkdir -p boot
	ld.lld -T link.ld -m elf_x86_64 -o boot/kernel.elf main.o

kernel.tar.gz: boot/kernel.elf
	tar czvf kernel.tar.gz boot/

kernel.img: kernel.tar.gz
	mkbootimg bootboot.json kernel.img

clean:
	rm -f *.o *.tar.gz *.img
	rm -rf out build boot

run:
	qemu-system-x86_64 -serial stdio kernel.img
